#!/bin/bash
#
#  gitwait - watch file and git commit all changes as they happen
#

while true; do

    inotifywait -qq -e CLOSE_WRITE /home/pi/workspace/van-travel-map/gps_data.location
    echo 'A change was detected in gps_data.location'
    cd /home/pi/workspace/van-travel-map/; git add gps_data.location; git commit -m 'autocommit for gps_data'; git push origin

done