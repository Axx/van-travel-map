from datetime import datetime
import plotly.graph_objects as go
import random as r
import pandas as pd
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import gpsd
import sched, time
from multiprocessing import Process, Queue
import os
from geopy import distance

poll_rate = 15 * 1000
minimum_radius_threshold_miles = 1
Last_Coordinate = None
gps_data_path = '/home/pi/workspace/van-travel-map/gps_data.location'

def write_location(s, lat_queue, lon_queue, text_queue):
    global Last_Coordinate

    packet_param = gpsd.get_current()
    if packet_param.mode >= 2:
        timestamp = str(packet_param.time)
        if timestamp.endswith('Z'):
            timestamp = timestamp[:-1] + '+00:00'
        text = datetime.fromisoformat(timestamp)
        next_coordinate = (packet_param.lat, packet_param.lon)
        print("Last Coordinate: {}".format(Last_Coordinate))
        if Last_Coordinate == None or distance.distance(Last_Coordinate, next_coordinate).miles > minimum_radius_threshold_miles:
            with open(gps_data_path, 'a') as file:
                file.write(text.isoformat() + ',' + str(packet_param.lat) + ',' + str(packet_param.lon) + '\n')
            print("[" + datetime.now().isoformat()+ "] writing to file: " + text.isoformat() + ',' + str(packet_param.lat) + ',' + str(packet_param.lon))
            print("Distance (miles): {} ".format(distance.distance(Last_Coordinate, next_coordinate).miles) + '\n')
            Last_Coordinate = (packet_param.lat, packet_param.lon)
            lon_queue.put(packet_param.lon)
            lat_queue.put(packet_param.lat)
            text_queue.put(text.isoformat() + '+00:00')
            
        elif Last_Coordinate != None:
            print("Coord 1: {} Coord 2: {} Distance (miles): {} ".format(Last_Coordinate, next_coordinate, distance.distance(Last_Coordinate, next_coordinate).miles) + '\n')

    #output_file.write(datetime.now().isoformat() + '+00:00' + ',' + str(lat) + ',' + str(long) + '\n')
    s.enter(poll_rate, 1, write_location, (s, lat_queue, lon_queue, text_queue))

def run_gpsd(lat_queue, lon_queue, text_queue):
    print("ID of process running run_gpsd program: {}".format(os.getpid()))
    s = sched.scheduler(time.time, time.sleep)
    # Connect somewhere else
    
    gpsd.connect()
    print("Run GPSD entered: ")
    s.enter(poll_rate, 1, write_location, (s, lat_queue, lon_queue, text_queue))
    s.run(blocking=True)

#Start Dash Server
def run_dash(lat_queue, lon_queue, text_queue):
    print("ID of process running run_dash program: {}".format(os.getpid()))
    df = pd.read_csv(gps_data_path)
    #Generate figure
    fig = go.Figure(
        data=go.Scattergeo(
            lat = df['latitude'],
            lon = df['longitude'],
            text = pd.to_datetime(df['timestamp']).dt.strftime('%B %d, %Y at %X'),
            mode = 'lines+markers',
            line = dict(width = 0.5,color = 'rgb(222, 79, 191)'),
            marker = dict(
                color = 'rgb(255, 29, 206)',
                opacity = 0.7,
                size = 5,
            )
        )
    )

    fig.update_layout(
        geo = dict(
            scope = 'north america',
            bgcolor = "rgb(36, 36, 36)",
            showland = True,
            landcolor = "rgb(64, 64, 64)",
            subunitcolor = "rgb(133, 133, 133)",
            countrycolor = "rgb(255, 255, 255)",
            showlakes = True,
            lakecolor = "rgb(36, 36, 36)",
            showsubunits = True,
            showcountries = True,
            resolution = 50,
            projection = dict(
                type = 'conic conformal',
                rotation_lon = -100
            )
        ),
        paper_bgcolor = "rgb(0, 0, 0)",
        plot_bgcolor = "rgb(36, 36, 36)",
        title=dict(
            text = "Lindy the Van's Travels",
            font = dict(
                color = "rgb(255, 255, 255)",
                family = "Arial",
                size = 32
            )
        ),
        height=700,
    )
    
    app = dash.Dash(__name__)
    app.layout = html.Div([
        dcc.Graph(id='lindyMap',
                figure=fig,
                config=dict(responsive=True),
                style={'width': '100%', 'height': '100%'}),
        dcc.Interval(id="interval", interval=1000 * poll_rate)
    ])

    @app.callback(Output('lindyMap', 'extendData'), [Input('interval', 'n_intervals')])
    def update_data(n_intervals):
        try:
            long_list = []
            lat_list = []
            text_list = []
            
            while lon_queue().qsize > 0:
                long = lon_queue.get(block=False)
                long_list.append(long)
                
                lat = lat_queue.get(block=False)
                lat_list.append(lat)
                
                text = text_queue.get(block=False)
                text_list.append(text)
                
                print("long: {}, lat: {}, text: {} adding data to graph.".format(long, lat, text))

            return dict(lon=[long_list], lat=[lat_list], text=[text_list])
        except:
            print("Exception in extendDate callback. Queue empty?")
            return dict(lon=[], lat=[])
        # tuple is (dict of new data, target trace index, number of points to keep)
           

    app.run_server(debug=False)
    

if __name__ == "__main__":
    lat_queue = Queue()
    lon_queue = Queue()
    text_queue = Queue()
    
    # print ID of current process
    print("ID of process running main program: {}".format(os.getpid()))
    print(__name__)

    with open(gps_data_path, 'r') as gps_data:
        for line in gps_data:
            pass

        data = line.split(',')
        if len(data) == 3 and data[1].isnumeric(): 
            Last_Coordinate = (data[1].strip(), data[2].strip())
            print("Last coordinate: {}".format(Last_Coordinate))
    
    gpsd_producer = Process(target=run_gpsd, args=(lat_queue, lon_queue, text_queue))
    dash_producer = Process(target=run_dash, args=(lat_queue, lon_queue, text_queue))
    
    gpsd_producer.start()
    dash_producer.start()
    
    dash_producer.join()
    gpsd_producer.join()
    
    
    print("Both processes completed.")
